$vboSize = 256MB
$iboSize = 128MB

$vboSizeOffset = 0x003A2DCC
$iboSizeOffset = 0x003A2DD6
$peHeaderTimestampOffset = 0x00000178

$currentFolder = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$wreckfestPath = $currentFolder + "\Wreckfest_x64.exe"
$wreckfestBackupPath = $currentFolder + "\Wreckfest_x64.exe.bu"
if([System.IO.File]::Exists($wreckfestPath))
{
	$bytes  = [System.IO.File]::ReadAllBytes($wreckfestPath)
	
	if 
	(
		($bytes[$peHeaderTimestampOffset + 0] -eq 0x89) -and
		($bytes[$peHeaderTimestampOffset + 1] -eq 0xE1) -and
		($bytes[$peHeaderTimestampOffset + 2] -eq 0xD0) -and
		($bytes[$peHeaderTimestampOffset + 3] -eq 0x5F)
	)
	{
		[System.IO.File]::WriteAllBytes($wreckfestBackupPath, $bytes)
		Write-Host "Backup created : " $wreckfestBackupPath

		#Write vbo size
		$bytes[$vboSizeOffset + 0] = ($vboSize -shr 0 -band 0xFF)	#LSB
		$bytes[$vboSizeOffset + 1] = ($vboSize -shr 8 -band 0xFF)
		$bytes[$vboSizeOffset + 2] = ($vboSize -shr 16 -band 0xFF)
		$bytes[$vboSizeOffset + 3] = ($vboSize -shr 24 -band 0xFF)	#MSB
		#Write ibo size
		$bytes[$iboSizeOffset + 0] = ($iboSize -shr 0 -band 0xFF)	#LSB
		$bytes[$iboSizeOffset + 1] = ($iboSize -shr 8 -band 0xFF)
		$bytes[$iboSizeOffset + 2] = ($iboSize -shr 16 -band 0xFF)
		$bytes[$iboSizeOffset + 3] = ($iboSize -shr 24 -band 0xFF)	#MSB

		[System.IO.File]::WriteAllBytes($wreckfestPath, $bytes)
		
		Write-Host "Exe patched : " $wreckfestPath
	}
	else
	{
		Write-Host "ERROR Version mismatch"
	}
}
else
{
	Write-Host "ERROR Wrong path : " $wreckfestPath
}

Write-Host "Press any key to exit..."
$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")