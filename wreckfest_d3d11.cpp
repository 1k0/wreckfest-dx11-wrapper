#include <windows.h>
#include <d3d11.h>
#include <stdlib.h>
#include <unordered_map>

#include "log.h"

typedef HRESULT (*PFD3D11CreateDeviceAndSwapChain) (
    IDXGIAdapter*,
    D3D_DRIVER_TYPE,
    HMODULE,
    UINT,
    const D3D_FEATURE_LEVEL*,
    UINT,
    UINT,
    const DXGI_SWAP_CHAIN_DESC*,
    IDXGISwapChain**,
    ID3D11Device**,
    D3D_FEATURE_LEVEL*,
    ID3D11DeviceContext**
);

typedef HRESULT (*PFD3D11CreateDevice) (
    IDXGIAdapter*,
    D3D_DRIVER_TYPE,
    HMODULE,
    UINT,
    const D3D_FEATURE_LEVEL*,
    UINT,
    UINT,
    ID3D11Device**,
    D3D_FEATURE_LEVEL*,
    ID3D11DeviceContext**
);

char* BufferUsageToString[] =
{
    "DEFAULT",
    "IMMUTABLE",
    "DYNAMIC",
    "STAGING"
};

char* BufferBindToString[] =
{
    "NONE",
    "VERTEX_BUFFER",
    "INDEX_BUFFER",
    "CONSTANT_BUFFER",
    "SHADER_RESOURCE",
    "STREAM_OUTPUT",
    "RENDER_TARGET",
    "DEPTH_STENCIL",
    "UNORDERED_ACCESS",
    "DECODER",
    "VIDEO_ENCODER"
};

char* BufferCpuAccessToString[] =
{
    "ACCESS_WRITE",
    "ACCESS_READ",
    "ACCESS_READWRITE"
};

char* BufferMiscToString[] = 
{
    "D3D11_RESOURCE_MISC_GENERATE_MIPS",
    "D3D11_RESOURCE_MISC_SHARED",
    "D3D11_RESOURCE_MISC_TEXTURECUBE",
    "D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS",
    "D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS",
    "D3D11_RESOURCE_MISC_BUFFER_STRUCTURED",
    "D3D11_RESOURCE_MISC_RESOURCE_CLAMP",
    "D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX",
    "D3D11_RESOURCE_MISC_GDI_COMPATIBLE",
    "D3D11_RESOURCE_MISC_SHARED_NTHANDLE",
    "D3D11_RESOURCE_MISC_RESTRICTED_CONTENT",
    "D3D11_RESOURCE_MISC_RESTRICT_SHARED_RESOURCE",
    "D3D11_RESOURCE_MISC_RESTRICT_SHARED_RESOURCE_DRIVER",
    "D3D11_RESOURCE_MISC_GUARDED",
    "D3D11_RESOURCE_MISC_TILE_POOL",
    "D3D11_RESOURCE_MISC_TILED",
    "D3D11_RESOURCE_MISC_HW_PROTECTED"
};

struct StagingBuffer
{
    void* buffer;
    bool dirty;

    StagingBuffer()
    {
        buffer = NULL;
        dirty = false;
    }

    void* Map()
    {
        return buffer;
    }

    void Unmap()
    {
        dirty = true;
    }

    void Init(msize size)
    {
        buffer = _aligned_malloc(size, 16);
        dirty = false;
    }

    void Finish()
    {
        if (buffer) _aligned_free(buffer);
    }
};
std::unordered_map<ID3D11Buffer*, StagingBuffer> gDynamicBuffers;

struct D3D11DeviceHook : ID3D11Device
{
public:
    ID3D11Device* mDevice;

    void Init(ID3D11Device* device)
    {
        mDevice = device;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(
        REFIID riid,
        _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject
    ) 
    {
        return mDevice->QueryInterface(riid, ppvObject);
    }

    ULONG STDMETHODCALLTYPE AddRef(void) 
    {
        return mDevice->AddRef();
    }

    ULONG STDMETHODCALLTYPE Release(void) 
    {
        return mDevice->Release();
    }

    HRESULT STDMETHODCALLTYPE CreateBuffer(
        _In_  const D3D11_BUFFER_DESC *pDesc,
        _In_opt_  const D3D11_SUBRESOURCE_DATA *pInitialData,
        _COM_Outptr_opt_  ID3D11Buffer **ppBuffer)
    {
        if (
            ((pDesc->Usage & D3D11_USAGE_DYNAMIC) && (pDesc->BindFlags & D3D11_BIND_VERTEX_BUFFER || pDesc->BindFlags & D3D11_BIND_INDEX_BUFFER)) &&
            (pDesc->ByteWidth != 2359296 && pDesc->ByteWidth != 114688 && pDesc->ByteWidth != 262144 && pDesc->ByteWidth != 3840000 && pDesc->ByteWidth != 120000)
        )
        {
            ((D3D11_BUFFER_DESC*) pDesc)->Usage = D3D11_USAGE_DEFAULT;
            ((D3D11_BUFFER_DESC*) pDesc)->CPUAccessFlags = 0;
            HRESULT result = mDevice->CreateBuffer(pDesc, pInitialData, ppBuffer);
            gDynamicBuffers[(*ppBuffer)].Init(pDesc->ByteWidth);

            //Frequently updated [I][CreateBuffer] Dynamic : 0000023B752ABED8, 2359296, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D8915DD18, 114688, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D89162CD8, 262144, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D891600D8, 3840000, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D89165098, 120000, INDEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D88B24898, 262144, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D89165358, 3840000, VERTEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000020D8915F5D8, 120000, INDEX_BUFFER, DEFAULT
            //[I][CreateBuffer] Dynamic : 0000018F2C5FFED8, 2359296, VERTEX_BUFFER, DEFAULT

            LOGI("[CreateBuffer] Dynamic : %p, %d, %s, %s", (*ppBuffer), pDesc->ByteWidth, BufferBindToString[pDesc->BindFlags], BufferUsageToString[pDesc->Usage]);
            return result;
        }
        
        /*
        if (pDesc->ByteWidth > (32 * 1024 * 1024))
        {
            if (pDesc->BindFlags & D3D11_BIND_VERTEX_BUFFER)
            {
                ((D3D11_BUFFER_DESC*) pDesc)->Usage = D3D11_USAGE_DEFAULT;
                ((D3D11_BUFFER_DESC*) pDesc)->CPUAccessFlags = 0;
                gVBOStaging = malloc(pDesc->ByteWidth);

                HRESULT result = mDevice->CreateBuffer(pDesc, pInitialData, ppBuffer);
                gVBO = (*ppBuffer);

                return result;
            }

            if (pDesc->BindFlags & D3D11_BIND_INDEX_BUFFER)
            {
                ((D3D11_BUFFER_DESC*)pDesc)->Usage = D3D11_USAGE_DEFAULT;
                ((D3D11_BUFFER_DESC*)pDesc)->CPUAccessFlags = 0;
                gIBOStaging = malloc(pDesc->ByteWidth);

                HRESULT result = mDevice->CreateBuffer(pDesc, pInitialData, ppBuffer);
                gIBO = (*ppBuffer);

                return result;
            }
        }*/
        HRESULT result = mDevice->CreateBuffer(pDesc, pInitialData, ppBuffer);
        LOGI("[CreateBuffer] Non-Dynamic : %p, %d, %s, %s", (*ppBuffer), pDesc->ByteWidth, BufferBindToString[pDesc->BindFlags], BufferUsageToString[pDesc->Usage]);
        return result;
    }

    HRESULT STDMETHODCALLTYPE CreateTexture1D(
        _In_  const D3D11_TEXTURE1D_DESC *pDesc,
        _In_reads_opt_(_Inexpressible_(pDesc->MipLevels * pDesc->ArraySize))  const D3D11_SUBRESOURCE_DATA *pInitialData,
        _COM_Outptr_opt_  ID3D11Texture1D **ppTexture1D)
    {
        return mDevice->CreateTexture1D(pDesc, pInitialData, ppTexture1D);
    }

    HRESULT STDMETHODCALLTYPE CreateTexture2D(
        _In_  const D3D11_TEXTURE2D_DESC *pDesc,
        _In_reads_opt_(_Inexpressible_(pDesc->MipLevels * pDesc->ArraySize))  const D3D11_SUBRESOURCE_DATA *pInitialData,
        _COM_Outptr_opt_  ID3D11Texture2D **ppTexture2D)
    { 
        return mDevice->CreateTexture2D(pDesc, pInitialData, ppTexture2D);
    }

     HRESULT STDMETHODCALLTYPE CreateTexture3D(
        _In_  const D3D11_TEXTURE3D_DESC *pDesc,
        _In_reads_opt_(_Inexpressible_(pDesc->MipLevels))  const D3D11_SUBRESOURCE_DATA *pInitialData,
        _COM_Outptr_opt_  ID3D11Texture3D **ppTexture3D)
     { 
         return mDevice->CreateTexture3D(pDesc, pInitialData, ppTexture3D);
     }

     HRESULT STDMETHODCALLTYPE CreateShaderResourceView(
        _In_  ID3D11Resource *pResource,
        _In_opt_  const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc,
        _COM_Outptr_opt_  ID3D11ShaderResourceView **ppSRView)
     { 
         return mDevice->CreateShaderResourceView(pResource, pDesc, ppSRView);
     }

     HRESULT STDMETHODCALLTYPE CreateUnorderedAccessView(
        _In_  ID3D11Resource *pResource,
        _In_opt_  const D3D11_UNORDERED_ACCESS_VIEW_DESC *pDesc,
        _COM_Outptr_opt_  ID3D11UnorderedAccessView **ppUAView)
     { 
         return mDevice->CreateUnorderedAccessView(pResource, pDesc, ppUAView);
     }

     HRESULT STDMETHODCALLTYPE CreateRenderTargetView(
        _In_  ID3D11Resource *pResource,
        _In_opt_  const D3D11_RENDER_TARGET_VIEW_DESC *pDesc,
        _COM_Outptr_opt_  ID3D11RenderTargetView **ppRTView)
     { 
         return mDevice->CreateRenderTargetView(pResource, pDesc, ppRTView);
     }

     HRESULT STDMETHODCALLTYPE CreateDepthStencilView(
        _In_  ID3D11Resource *pResource,
        _In_opt_  const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc,
        _COM_Outptr_opt_  ID3D11DepthStencilView **ppDepthStencilView)
     {
         return mDevice->CreateDepthStencilView(pResource, pDesc, ppDepthStencilView);
     }

     HRESULT STDMETHODCALLTYPE CreateInputLayout(
        _In_reads_(NumElements)  const D3D11_INPUT_ELEMENT_DESC *pInputElementDescs,
        _In_range_(0, D3D11_IA_VERTEX_INPUT_STRUCTURE_ELEMENT_COUNT)  UINT NumElements,
        _In_reads_(BytecodeLength)  const void *pShaderBytecodeWithInputSignature,
        _In_  SIZE_T BytecodeLength,
        _COM_Outptr_opt_  ID3D11InputLayout **ppInputLayout)
     { 
         return mDevice->CreateInputLayout(pInputElementDescs, NumElements, pShaderBytecodeWithInputSignature, BytecodeLength, ppInputLayout);
     }

     HRESULT STDMETHODCALLTYPE CreateVertexShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11VertexShader **ppVertexShader)
     {
         return mDevice->CreateVertexShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppVertexShader);
     }

     HRESULT STDMETHODCALLTYPE CreateGeometryShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11GeometryShader **ppGeometryShader)
     { 
         return mDevice->CreateGeometryShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppGeometryShader);
     }

     HRESULT STDMETHODCALLTYPE CreateGeometryShaderWithStreamOutput(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_reads_opt_(NumEntries)  const D3D11_SO_DECLARATION_ENTRY *pSODeclaration,
        _In_range_(0, D3D11_SO_STREAM_COUNT * D3D11_SO_OUTPUT_COMPONENT_COUNT)  UINT NumEntries,
        _In_reads_opt_(NumStrides)  const UINT *pBufferStrides,
        _In_range_(0, D3D11_SO_BUFFER_SLOT_COUNT)  UINT NumStrides,
        _In_  UINT RasterizedStream,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11GeometryShader **ppGeometryShader)
     {
         return mDevice->CreateGeometryShaderWithStreamOutput(
             pShaderBytecode,
             BytecodeLength,
             pSODeclaration,
             NumEntries,
             pBufferStrides,
             NumStrides,
             RasterizedStream,
             pClassLinkage,
             ppGeometryShader
         );
     }

     HRESULT STDMETHODCALLTYPE CreatePixelShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11PixelShader **ppPixelShader)
     { 
         return mDevice->CreatePixelShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppPixelShader);
     }

     HRESULT STDMETHODCALLTYPE CreateHullShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11HullShader **ppHullShader)
     { 
         return mDevice->CreateHullShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppHullShader);
     }

     HRESULT STDMETHODCALLTYPE CreateDomainShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11DomainShader **ppDomainShader)
     { 
         return mDevice->CreateDomainShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppDomainShader);
     }

     HRESULT STDMETHODCALLTYPE CreateComputeShader(
        _In_reads_(BytecodeLength)  const void *pShaderBytecode,
        _In_  SIZE_T BytecodeLength,
        _In_opt_  ID3D11ClassLinkage *pClassLinkage,
        _COM_Outptr_opt_  ID3D11ComputeShader **ppComputeShader)
     {
         return mDevice->CreateComputeShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppComputeShader);
     }

     HRESULT STDMETHODCALLTYPE CreateClassLinkage(
        _COM_Outptr_  ID3D11ClassLinkage **ppLinkage)
     {
         return mDevice->CreateClassLinkage(ppLinkage);
     }

     HRESULT STDMETHODCALLTYPE CreateBlendState(
        _In_  const D3D11_BLEND_DESC *pBlendStateDesc,
        _COM_Outptr_opt_  ID3D11BlendState **ppBlendState)
     { 
         return mDevice->CreateBlendState(pBlendStateDesc, ppBlendState);
     }

     HRESULT STDMETHODCALLTYPE CreateDepthStencilState(
        _In_  const D3D11_DEPTH_STENCIL_DESC *pDepthStencilDesc,
        _COM_Outptr_opt_  ID3D11DepthStencilState **ppDepthStencilState)
     {
         return mDevice->CreateDepthStencilState(pDepthStencilDesc, ppDepthStencilState);
     }

     HRESULT STDMETHODCALLTYPE CreateRasterizerState(
        _In_  const D3D11_RASTERIZER_DESC *pRasterizerDesc,
        _COM_Outptr_opt_  ID3D11RasterizerState **ppRasterizerState)
     { 
         return mDevice->CreateRasterizerState(pRasterizerDesc, ppRasterizerState);
     }

     HRESULT STDMETHODCALLTYPE CreateSamplerState(
        _In_  const D3D11_SAMPLER_DESC *pSamplerDesc,
        _COM_Outptr_opt_  ID3D11SamplerState **ppSamplerState)
     {
         return mDevice->CreateSamplerState(pSamplerDesc, ppSamplerState);
     }

     HRESULT STDMETHODCALLTYPE CreateQuery(
        _In_  const D3D11_QUERY_DESC *pQueryDesc,
        _COM_Outptr_opt_  ID3D11Query **ppQuery)
     {
         return mDevice->CreateQuery(pQueryDesc, ppQuery);
     }

     HRESULT STDMETHODCALLTYPE CreatePredicate(
        _In_  const D3D11_QUERY_DESC *pPredicateDesc,
        _COM_Outptr_opt_  ID3D11Predicate **ppPredicate)
     {
         return mDevice->CreatePredicate(pPredicateDesc, ppPredicate);
     }

     HRESULT STDMETHODCALLTYPE CreateCounter(
        _In_  const D3D11_COUNTER_DESC *pCounterDesc,
        _COM_Outptr_opt_  ID3D11Counter **ppCounter)
     {
         return mDevice->CreateCounter(pCounterDesc, ppCounter);
     }

     HRESULT STDMETHODCALLTYPE CreateDeferredContext(
        UINT ContextFlags,
        _COM_Outptr_opt_  ID3D11DeviceContext **ppDeferredContext)
     {
         return mDevice->CreateDeferredContext(ContextFlags, ppDeferredContext);
     }

     HRESULT STDMETHODCALLTYPE OpenSharedResource(
        _In_  HANDLE hResource,
        _In_  REFIID ReturnedInterface,
        _COM_Outptr_opt_  void **ppResource)
     { 
         return mDevice->OpenSharedResource(hResource, ReturnedInterface, ppResource);
     }

     HRESULT STDMETHODCALLTYPE CheckFormatSupport(
        _In_  DXGI_FORMAT Format,
        _Out_  UINT *pFormatSupport)
     {
         return mDevice->CheckFormatSupport(Format, pFormatSupport);
     }

     HRESULT STDMETHODCALLTYPE CheckMultisampleQualityLevels(
        _In_  DXGI_FORMAT Format,
        _In_  UINT SampleCount,
        _Out_  UINT *pNumQualityLevels)
     { 
         return mDevice->CheckMultisampleQualityLevels(Format, SampleCount, pNumQualityLevels);
     }

     void STDMETHODCALLTYPE CheckCounterInfo(
        _Out_  D3D11_COUNTER_INFO *pCounterInfo)
     { 
         return mDevice->CheckCounterInfo(pCounterInfo);
     }

     HRESULT STDMETHODCALLTYPE CheckCounter(
        _In_  const D3D11_COUNTER_DESC *pDesc,
        _Out_  D3D11_COUNTER_TYPE *pType,
        _Out_  UINT *pActiveCounters,
        _Out_writes_opt_(*pNameLength)  LPSTR szName,
        _Inout_opt_  UINT *pNameLength,
        _Out_writes_opt_(*pUnitsLength)  LPSTR szUnits,
        _Inout_opt_  UINT *pUnitsLength,
        _Out_writes_opt_(*pDescriptionLength)  LPSTR szDescription,
        _Inout_opt_  UINT *pDescriptionLength)
     { 
         return mDevice->CheckCounter(
             pDesc,
             pType,
             pActiveCounters,
             szName,
             pNameLength,
             szUnits, pUnitsLength, 
             szDescription, 
             pDescriptionLength
         );
     }

     HRESULT STDMETHODCALLTYPE CheckFeatureSupport(
        D3D11_FEATURE Feature,
        _Out_writes_bytes_(FeatureSupportDataSize)  void *pFeatureSupportData,
        UINT FeatureSupportDataSize)
     {
         return mDevice->CheckFeatureSupport(Feature, pFeatureSupportData, FeatureSupportDataSize);
     }

     HRESULT STDMETHODCALLTYPE GetPrivateData(
        _In_  REFGUID guid,
        _Inout_  UINT *pDataSize,
        _Out_writes_bytes_opt_(*pDataSize)  void *pData)
     { 
         return mDevice->GetPrivateData(guid, pDataSize, pData);
     }

     HRESULT STDMETHODCALLTYPE SetPrivateData(
        _In_  REFGUID guid,
        _In_  UINT DataSize,
        _In_reads_bytes_opt_(DataSize)  const void *pData)
     { 
         return mDevice->SetPrivateData(guid, DataSize, pData);
     }

     HRESULT STDMETHODCALLTYPE SetPrivateDataInterface(
        _In_  REFGUID guid,
        _In_opt_  const IUnknown *pData)
     {
         return mDevice->SetPrivateDataInterface(guid, pData);
     }

     D3D_FEATURE_LEVEL STDMETHODCALLTYPE GetFeatureLevel(void)
     { 
         return mDevice->GetFeatureLevel();
     }

     UINT STDMETHODCALLTYPE GetCreationFlags(void)
     {
         return mDevice->GetCreationFlags();
     }

     HRESULT STDMETHODCALLTYPE GetDeviceRemovedReason(void)
     { 
         return mDevice->GetDeviceRemovedReason();
     }

     void STDMETHODCALLTYPE GetImmediateContext(
        _Outptr_  ID3D11DeviceContext **ppImmediateContext)
     { 
         mDevice->GetImmediateContext(ppImmediateContext);
     }

     HRESULT STDMETHODCALLTYPE SetExceptionMode(UINT RaiseFlags)
     { 
         return mDevice->SetExceptionMode(RaiseFlags);
     }

     UINT STDMETHODCALLTYPE GetExceptionMode(void)
     { 
         return mDevice->GetExceptionMode();
     }
};

struct D3D11DeviceContext : ID3D11DeviceContext
{
public : 
    ID3D11DeviceContext* mContext;

    void Init(ID3D11DeviceContext* context)
    {
        mContext = context;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(
        REFIID riid,
        _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject
    )
    {
        return mContext->QueryInterface(riid, ppvObject);
    }

    ULONG STDMETHODCALLTYPE AddRef(void)
    {
        return mContext->AddRef();
    }

    ULONG STDMETHODCALLTYPE Release(void)
    {
        return mContext->Release();
    }

    void STDMETHODCALLTYPE GetDevice(
        ID3D11Device **ppDevice)
    {
        mContext->GetDevice(ppDevice);
    }

    HRESULT STDMETHODCALLTYPE GetPrivateData(
        REFGUID guid,
        UINT *pDataSize,
        void *pData)
    {
        return mContext->GetPrivateData(guid, pDataSize, pData);
    }

    HRESULT STDMETHODCALLTYPE SetPrivateData(
        REFGUID guid,
        UINT DataSize,
        const void *pData)
    {
        return mContext->SetPrivateData(guid, DataSize, pData);
    }

    HRESULT STDMETHODCALLTYPE SetPrivateDataInterface(
        REFGUID guid,
        const IUnknown *pData)
    {
        return mContext->SetPrivateDataInterface(guid, pData);
    }

    void STDMETHODCALLTYPE VSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers
    )
    {
        mContext->VSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE PSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews
    )
    {
        mContext->PSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE PSSetShader(
        ID3D11PixelShader *pPixelShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->PSSetShader(pPixelShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE PSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers) 
    {
        mContext->PSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE VSSetShader(
        ID3D11VertexShader *pVertexShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->VSSetShader(pVertexShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE DrawIndexed(
        UINT IndexCount,
        UINT StartIndexLocation,
        INT BaseVertexLocation)
    {
        mContext->DrawIndexed(IndexCount, StartIndexLocation, BaseVertexLocation);
    }

    void STDMETHODCALLTYPE Draw(
        UINT VertexCount,
        UINT StartVertexLocation)
    {
        mContext->Draw(VertexCount, StartVertexLocation);
    }

    HRESULT STDMETHODCALLTYPE Map(
        ID3D11Resource *pResource,
        UINT Subresource,
        D3D11_MAP MapType,
        UINT MapFlags,
        D3D11_MAPPED_SUBRESOURCE *pMappedResource)
    {
        auto& result = gDynamicBuffers.find((ID3D11Buffer*) pResource);
        if (result != gDynamicBuffers.end())
        {
            //LOGI("[Map] Dynamic Buffer : %p", (*result).first);
            pMappedResource->pData = result->second.Map();
            pMappedResource->RowPitch = 0;
            pMappedResource->DepthPitch = 0;

            return S_OK;
        }

        return mContext->Map(pResource, Subresource, MapType, MapFlags, pMappedResource);
    }

    void STDMETHODCALLTYPE Unmap(
        ID3D11Resource *pResource,
        UINT Subresource)
    {
        auto result = gDynamicBuffers.find((ID3D11Buffer*) pResource);
        if (result != gDynamicBuffers.end())
        {
            result->second.Unmap();
            return;
        }

        mContext->Unmap(pResource, Subresource);

        for (auto& kvp : gDynamicBuffers)
        {
            if (kvp.second.dirty)
            {
                LOGI("[Unmap] Triggerer : %p Uploading dirty buffer : %p", pResource, kvp.first);
                mContext->UpdateSubresource(kvp.first, 0, NULL, kvp.second.buffer, 0, 0);
                kvp.second.dirty = false;
            }
        }
    }

    void STDMETHODCALLTYPE PSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers)
    {
        mContext->PSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE IASetInputLayout(
        ID3D11InputLayout *pInputLayout)
    {
        mContext->IASetInputLayout(pInputLayout);
    }

    void STDMETHODCALLTYPE IASetVertexBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppVertexBuffers,
        const UINT *pStrides,
        const UINT *pOffsets)
    {
        mContext->IASetVertexBuffers(StartSlot, NumBuffers, ppVertexBuffers, pStrides, pOffsets);
    }

    void STDMETHODCALLTYPE IASetIndexBuffer(
        ID3D11Buffer *pIndexBuffer,
        DXGI_FORMAT Format,
        UINT Offset)
    {
        mContext->IASetIndexBuffer(pIndexBuffer, Format, Offset);
    }

    void STDMETHODCALLTYPE DrawIndexedInstanced(
        UINT IndexCountPerInstance,
        UINT InstanceCount,
        UINT StartIndexLocation,
        INT BaseVertexLocation,
        UINT StartInstanceLocation)
    {
        mContext->DrawIndexedInstanced(
            IndexCountPerInstance, 
            InstanceCount, 
            StartIndexLocation, 
            BaseVertexLocation, 
            StartInstanceLocation);
    }

    void STDMETHODCALLTYPE DrawInstanced(
        UINT VertexCountPerInstance,
        UINT InstanceCount,
        UINT StartVertexLocation,
        UINT StartInstanceLocation)
    {
        mContext->DrawInstanced(VertexCountPerInstance, InstanceCount, StartVertexLocation, StartInstanceLocation);
    }

    void STDMETHODCALLTYPE GSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers)
    {
        mContext->GSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE GSSetShader(
        ID3D11GeometryShader *pShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->GSSetShader(pShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE IASetPrimitiveTopology(
        D3D11_PRIMITIVE_TOPOLOGY Topology)
    {
        mContext->IASetPrimitiveTopology(Topology);
    }

    void STDMETHODCALLTYPE VSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews)
    {
        mContext->VSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE VSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers)
    {
        mContext->VSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE Begin(
        ID3D11Asynchronous *pAsync)
    {
        mContext->Begin(pAsync);
    }

    void STDMETHODCALLTYPE End(
        ID3D11Asynchronous *pAsync)
    {
        mContext->End(pAsync);
    }

    HRESULT STDMETHODCALLTYPE GetData(
        ID3D11Asynchronous *pAsync,
        void *pData,
        UINT DataSize,
        UINT GetDataFlags)
    {
        return mContext->GetData(pAsync, pData, DataSize, GetDataFlags);
    }

    void STDMETHODCALLTYPE SetPredication(
        ID3D11Predicate *pPredicate,
        BOOL PredicateValue)
    {
        mContext->SetPredication(pPredicate, PredicateValue);
    }

    void STDMETHODCALLTYPE GSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews)
    {
        mContext->GSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE GSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers)
    {
        mContext->GSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE OMSetRenderTargets(
        UINT NumViews,
        ID3D11RenderTargetView *const *ppRenderTargetViews,
        ID3D11DepthStencilView *pDepthStencilView)
    {
        mContext->OMSetRenderTargets(NumViews, ppRenderTargetViews, pDepthStencilView);
    }

    void STDMETHODCALLTYPE OMSetRenderTargetsAndUnorderedAccessViews(
        UINT NumRTVs,
        ID3D11RenderTargetView *const *ppRenderTargetViews,
        ID3D11DepthStencilView *pDepthStencilView,
        UINT UAVStartSlot,
        UINT NumUAVs,
        ID3D11UnorderedAccessView *const *ppUnorderedAccessViews,
        const UINT *pUAVInitialCounts)
    {
        mContext->OMSetRenderTargetsAndUnorderedAccessViews(
            NumRTVs, 
            ppRenderTargetViews, 
            pDepthStencilView, 
            UAVStartSlot, 
            NumUAVs, 
            ppUnorderedAccessViews, 
            pUAVInitialCounts
        );
    }

    void STDMETHODCALLTYPE OMSetBlendState(
        ID3D11BlendState *pBlendState,
        const FLOAT BlendFactor[4],
        UINT SampleMask)
    {
        mContext->OMSetBlendState(pBlendState, BlendFactor, SampleMask);
    }

    void STDMETHODCALLTYPE OMSetDepthStencilState(
        ID3D11DepthStencilState *pDepthStencilState,
        UINT StencilRef)
    {
        mContext->OMSetDepthStencilState(pDepthStencilState, StencilRef);
    }

    void STDMETHODCALLTYPE SOSetTargets(
        UINT NumBuffers,
        ID3D11Buffer *const *ppSOTargets,
        const UINT *pOffsets)
    {
        mContext->SOSetTargets(NumBuffers, ppSOTargets, pOffsets);
    }

    void STDMETHODCALLTYPE DrawAuto(void)
    {
        mContext->DrawAuto();
    }

    void STDMETHODCALLTYPE DrawIndexedInstancedIndirect(
        ID3D11Buffer *pBufferForArgs,
        UINT AlignedByteOffsetForArgs)
    {
        mContext->DrawIndexedInstancedIndirect(pBufferForArgs, AlignedByteOffsetForArgs);
    }

    void STDMETHODCALLTYPE DrawInstancedIndirect(
        ID3D11Buffer *pBufferForArgs,
        UINT AlignedByteOffsetForArgs)
    {
        mContext->DrawInstancedIndirect(pBufferForArgs, AlignedByteOffsetForArgs);
    }

    void STDMETHODCALLTYPE Dispatch(
        UINT ThreadGroupCountX,
        UINT ThreadGroupCountY,
        UINT ThreadGroupCountZ)
    {
        mContext->Dispatch(ThreadGroupCountX, ThreadGroupCountY, ThreadGroupCountZ);
    }

    void STDMETHODCALLTYPE DispatchIndirect(
        ID3D11Buffer *pBufferForArgs,
        UINT AlignedByteOffsetForArgs)
    {
        mContext->DispatchIndirect(pBufferForArgs, AlignedByteOffsetForArgs);
    }

    void STDMETHODCALLTYPE RSSetState(
        ID3D11RasterizerState *pRasterizerState)
    {
        mContext->RSSetState(pRasterizerState);
    }

    void STDMETHODCALLTYPE RSSetViewports(
        UINT NumViewports,
        const D3D11_VIEWPORT *pViewports)
    {
        mContext->RSSetViewports(NumViewports, pViewports);
    }

    void STDMETHODCALLTYPE RSSetScissorRects(
        UINT NumRects,
        const D3D11_RECT *pRects) 
    {
        mContext->RSSetScissorRects(NumRects, pRects);
    }

    void STDMETHODCALLTYPE CopySubresourceRegion(
        ID3D11Resource *pDstResource,
        UINT DstSubresource,
        UINT DstX,
        UINT DstY,
        UINT DstZ,
        ID3D11Resource *pSrcResource,
        UINT SrcSubresource,
        const D3D11_BOX *pSrcBox)
    {
        mContext->CopySubresourceRegion(pDstResource, DstSubresource, DstX, DstY, DstZ, pSrcResource, SrcSubresource, pSrcBox);
    }

    void STDMETHODCALLTYPE CopyResource(
        ID3D11Resource *pDstResource,
        ID3D11Resource *pSrcResource)
    {
        mContext->CopyResource(pDstResource, pSrcResource);
    }

    void STDMETHODCALLTYPE UpdateSubresource(
        ID3D11Resource *pDstResource,
        UINT DstSubresource,
        const D3D11_BOX *pDstBox,
        const void *pSrcData,
        UINT SrcRowPitch,
        UINT SrcDepthPitch)
    {
        mContext->UpdateSubresource(pDstResource, DstSubresource, pDstBox, pSrcData, SrcRowPitch, SrcDepthPitch);
    }

    void STDMETHODCALLTYPE CopyStructureCount(
        ID3D11Buffer *pDstBuffer,
        UINT DstAlignedByteOffset,
        ID3D11UnorderedAccessView *pSrcView)
    {
        mContext->CopyStructureCount(pDstBuffer, DstAlignedByteOffset, pSrcView);
    }

    void STDMETHODCALLTYPE ClearRenderTargetView(
        ID3D11RenderTargetView *pRenderTargetView,
        const FLOAT ColorRGBA[4])
    {
        mContext->ClearRenderTargetView(pRenderTargetView, ColorRGBA);
    }

    void STDMETHODCALLTYPE ClearUnorderedAccessViewUint(
        ID3D11UnorderedAccessView *pUnorderedAccessView,
        const UINT Values[4])
    {
        mContext->ClearUnorderedAccessViewUint(pUnorderedAccessView, Values);
    }

    void STDMETHODCALLTYPE ClearUnorderedAccessViewFloat(
        ID3D11UnorderedAccessView *pUnorderedAccessView,
        const FLOAT Values[4])
    {
        mContext->ClearUnorderedAccessViewFloat(pUnorderedAccessView, Values);
    }

    void STDMETHODCALLTYPE ClearDepthStencilView(
        ID3D11DepthStencilView *pDepthStencilView,
        UINT ClearFlags,
        FLOAT Depth,
        UINT8 Stencil)
    {
        mContext->ClearDepthStencilView(pDepthStencilView, ClearFlags, Depth, Stencil);
    }

    void STDMETHODCALLTYPE GenerateMips(
        ID3D11ShaderResourceView *pShaderResourceView)
    {
        mContext->GenerateMips(pShaderResourceView);
    }

    void STDMETHODCALLTYPE SetResourceMinLOD(
        ID3D11Resource *pResource,
        FLOAT MinLOD)
    {
        mContext->SetResourceMinLOD(pResource, MinLOD);
    }

    FLOAT STDMETHODCALLTYPE GetResourceMinLOD(
        ID3D11Resource *pResource)
    {
        return mContext->GetResourceMinLOD(pResource);
    }

    void STDMETHODCALLTYPE ResolveSubresource(
        ID3D11Resource *pDstResource,
        UINT DstSubresource,
        ID3D11Resource *pSrcResource,
        UINT SrcSubresource,
        DXGI_FORMAT Format)
    {
        mContext->ResolveSubresource(pDstResource, DstSubresource, pSrcResource, SrcSubresource, Format);
    }

    void STDMETHODCALLTYPE ExecuteCommandList(
        ID3D11CommandList *pCommandList,
        BOOL RestoreContextState) 
    {
        mContext->ExecuteCommandList(pCommandList, RestoreContextState);
    }

    void STDMETHODCALLTYPE HSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews)
    {
        mContext->HSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE HSSetShader(
        ID3D11HullShader *pHullShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->HSSetShader(pHullShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE HSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers)
    {
        mContext->HSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE HSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers)
    {
        mContext->HSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE DSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews)
    {
        mContext->DSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE DSSetShader(
        ID3D11DomainShader *pDomainShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->DSSetShader(pDomainShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE DSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers)
    {
        mContext->DSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE DSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers)
    {
        mContext->DSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE CSSetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView *const *ppShaderResourceViews)
    {
        mContext->CSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE CSSetUnorderedAccessViews(
        UINT StartSlot,
        UINT NumUAVs,
        ID3D11UnorderedAccessView *const *ppUnorderedAccessViews,
        const UINT *pUAVInitialCounts)
    {
        mContext->CSSetUnorderedAccessViews(StartSlot, NumUAVs, ppUnorderedAccessViews, pUAVInitialCounts);
    }

    void STDMETHODCALLTYPE CSSetShader(
        ID3D11ComputeShader *pComputeShader,
        ID3D11ClassInstance *const *ppClassInstances,
        UINT NumClassInstances)
    {
        mContext->CSSetShader(pComputeShader, ppClassInstances, NumClassInstances);
    }

    void STDMETHODCALLTYPE CSSetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState *const *ppSamplers)
    {
        mContext->CSSetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE CSSetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer *const *ppConstantBuffers)
    {
        mContext->CSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE VSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->VSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE PSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->PSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE PSGetShader(
        ID3D11PixelShader **ppPixelShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->PSGetShader(ppPixelShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE PSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->PSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE VSGetShader(
        ID3D11VertexShader **ppVertexShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->VSGetShader(ppVertexShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE PSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->PSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE IAGetInputLayout(
        ID3D11InputLayout **ppInputLayout)
    {
        mContext->IAGetInputLayout(ppInputLayout);
    }

    void STDMETHODCALLTYPE IAGetVertexBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppVertexBuffers,
        UINT *pStrides,
        UINT *pOffsets)
    {
        mContext->IAGetVertexBuffers(StartSlot, NumBuffers, ppVertexBuffers, pStrides, pOffsets);
    }

    void STDMETHODCALLTYPE IAGetIndexBuffer(
        ID3D11Buffer **pIndexBuffer,
        DXGI_FORMAT *Format,
        UINT *Offset)
    {
        mContext->IAGetIndexBuffer(pIndexBuffer, Format, Offset);
    }

    void STDMETHODCALLTYPE GSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->GSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE GSGetShader(
        ID3D11GeometryShader **ppGeometryShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->GSGetShader(ppGeometryShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE IAGetPrimitiveTopology(
        D3D11_PRIMITIVE_TOPOLOGY *pTopology)
    {
        mContext->IAGetPrimitiveTopology(pTopology);
    }

    void STDMETHODCALLTYPE VSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->VSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE VSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->VSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE GetPredication(
        ID3D11Predicate **ppPredicate,
        BOOL *pPredicateValue)
    {
        mContext->GetPredication(ppPredicate, pPredicateValue);
    }

    void STDMETHODCALLTYPE GSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->GSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE GSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->GSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE OMGetRenderTargets(
        UINT NumViews,
        ID3D11RenderTargetView **ppRenderTargetViews,
        ID3D11DepthStencilView **ppDepthStencilView)
    {
        mContext->OMGetRenderTargets(NumViews, ppRenderTargetViews, ppDepthStencilView);
    }

    void STDMETHODCALLTYPE OMGetRenderTargetsAndUnorderedAccessViews(
        UINT NumRTVs,
        ID3D11RenderTargetView **ppRenderTargetViews,
        ID3D11DepthStencilView **ppDepthStencilView,
        UINT UAVStartSlot,
        UINT NumUAVs,
        ID3D11UnorderedAccessView **ppUnorderedAccessViews)
    {
        mContext->OMGetRenderTargetsAndUnorderedAccessViews(
            NumRTVs, 
            ppRenderTargetViews, 
            ppDepthStencilView, 
            UAVStartSlot, 
            NumUAVs, 
            ppUnorderedAccessViews
        );
    }

    void STDMETHODCALLTYPE OMGetBlendState(
        ID3D11BlendState **ppBlendState,
        FLOAT BlendFactor[4],
        UINT *pSampleMask)
    {
        mContext->OMGetBlendState(ppBlendState, BlendFactor, pSampleMask);
    }

    void STDMETHODCALLTYPE OMGetDepthStencilState(
        ID3D11DepthStencilState **ppDepthStencilState,
        UINT *pStencilRef)
    {
        mContext->OMGetDepthStencilState(ppDepthStencilState, pStencilRef);
    }

    void STDMETHODCALLTYPE SOGetTargets(
        UINT NumBuffers,
        ID3D11Buffer **ppSOTargets)
    {
        mContext->SOGetTargets(NumBuffers, ppSOTargets);
    }

    void STDMETHODCALLTYPE RSGetState(
        ID3D11RasterizerState **ppRasterizerState)
    {
        mContext->RSGetState(ppRasterizerState);
    }

    void STDMETHODCALLTYPE RSGetViewports(
        UINT *pNumViewports,
        D3D11_VIEWPORT *pViewports)
    {
        mContext->RSGetViewports(pNumViewports, pViewports);
    }

    void STDMETHODCALLTYPE RSGetScissorRects(
        UINT *pNumRects,
        D3D11_RECT *pRects)
    {
        mContext->RSGetScissorRects(pNumRects, pRects);
    }

    void STDMETHODCALLTYPE HSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->HSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE HSGetShader(
        ID3D11HullShader **ppHullShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->HSGetShader(ppHullShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE HSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->HSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE HSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->HSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE DSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->DSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE DSGetShader(
        ID3D11DomainShader **ppDomainShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->DSGetShader(ppDomainShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE DSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->DSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE DSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->DSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE CSGetShaderResources(
        UINT StartSlot,
        UINT NumViews,
        ID3D11ShaderResourceView **ppShaderResourceViews)
    {
        mContext->CSGetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
    }

    void STDMETHODCALLTYPE CSGetUnorderedAccessViews(
        UINT StartSlot,
        UINT NumUAVs,
        ID3D11UnorderedAccessView **ppUnorderedAccessViews)
    {
        mContext->CSGetUnorderedAccessViews(StartSlot, NumUAVs, ppUnorderedAccessViews);
    }

    void STDMETHODCALLTYPE CSGetShader(
        ID3D11ComputeShader **ppComputeShader,
        ID3D11ClassInstance **ppClassInstances,
        UINT *pNumClassInstances)
    {
        mContext->CSGetShader(ppComputeShader, ppClassInstances, pNumClassInstances);
    }

    void STDMETHODCALLTYPE CSGetSamplers(
        UINT StartSlot,
        UINT NumSamplers,
        ID3D11SamplerState **ppSamplers)
    {
        mContext->CSGetSamplers(StartSlot, NumSamplers, ppSamplers);
    }

    void STDMETHODCALLTYPE CSGetConstantBuffers(
        UINT StartSlot,
        UINT NumBuffers,
        ID3D11Buffer **ppConstantBuffers)
    {
        mContext->CSGetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
    }

    void STDMETHODCALLTYPE ClearState(void)
    {
        mContext->ClearState();
    }

    void STDMETHODCALLTYPE Flush(void)
    {
        mContext->Flush();
    }

    D3D11_DEVICE_CONTEXT_TYPE STDMETHODCALLTYPE GetType(void)
    {
        return mContext->GetType();
    }

    UINT STDMETHODCALLTYPE GetContextFlags(void)
    {
        return mContext->GetContextFlags();
    }

    HRESULT STDMETHODCALLTYPE FinishCommandList(
        BOOL RestoreDeferredContextState,
        ID3D11CommandList **ppCommandList)
    {
        return mContext->FinishCommandList(RestoreDeferredContextState, ppCommandList);
    }
};

static HMODULE gD3D11DLL = NULL;
static PFD3D11CreateDeviceAndSwapChain gD3D11CreateDeviceAndSwapChain;
static PFD3D11CreateDevice gD3D11CreateDevice;
static D3D11DeviceHook gDeviceHook;
static D3D11DeviceContext gContextHook;

BOOL APIENTRY DllMain(HMODULE module, DWORD event, LPVOID reserved)
{
    switch (event)
    {

    case DLL_PROCESS_ATTACH :
    {
        LogInit(LOG_TARGET_NONE, "dx11_log.txt"); 
        gD3D11DLL = LoadLibraryA("C:\\Windows\\System32\\d3d11.dll");
        if (gD3D11DLL)
        {
            LOGI("[MAIN] D3D11 DLL loaded");

            gD3D11CreateDeviceAndSwapChain = (PFD3D11CreateDeviceAndSwapChain) GetProcAddress(gD3D11DLL, "D3D11CreateDeviceAndSwapChain");
            gD3D11CreateDevice = (PFD3D11CreateDevice) GetProcAddress(gD3D11DLL, "D3D11CreateDevice");
        }
    }
    break;
    
    case DLL_PROCESS_DETACH :
    {
        LogFinish();
    }
    break;

    }

    return TRUE;
}

HRESULT APIENTRY API_D3D11CreateDeviceAndSwapChain(
    IDXGIAdapter               *pAdapter,
    D3D_DRIVER_TYPE            DriverType,
    HMODULE                    Software,
    UINT                       Flags,
    const D3D_FEATURE_LEVEL    *pFeatureLevels,
    UINT                       FeatureLevels,
    UINT                       SDKVersion,
    const DXGI_SWAP_CHAIN_DESC *pSwapChainDesc,
    IDXGISwapChain             **ppSwapChain,
    ID3D11Device               **ppDevice,
    D3D_FEATURE_LEVEL          *pFeatureLevel,
    ID3D11DeviceContext        **ppImmediateContext
)
{
    LOGI("[API] D3D11CreateDeviceAndSwapChain");

    HRESULT result = gD3D11CreateDeviceAndSwapChain(
        pAdapter,
        DriverType,
        Software,
        Flags,
        pFeatureLevels,
        FeatureLevels,
        SDKVersion,
        pSwapChainDesc,
        ppSwapChain,
        ppDevice,
        pFeatureLevel,
        ppImmediateContext
    );
    gDeviceHook.Init(*ppDevice);
    (*ppDevice) = (ID3D11Device*) &gDeviceHook;
    gContextHook.Init(*ppImmediateContext);
    (*ppImmediateContext) = (ID3D11DeviceContext*) &gContextHook;

    return result;
}

HRESULT APIENTRY API_D3D11CreateDevice(
    IDXGIAdapter            *pAdapter,
    D3D_DRIVER_TYPE         DriverType,
    HMODULE                 Software,
    UINT                    Flags,
    const D3D_FEATURE_LEVEL *pFeatureLevels,
    UINT                    FeatureLevels,
    UINT                    SDKVersion,
    ID3D11Device            **ppDevice,
    D3D_FEATURE_LEVEL       *pFeatureLevel,
    ID3D11DeviceContext     **ppImmediateContext
)
{
    LOGI("[API] D3D11CreateDevice");

    HRESULT result = gD3D11CreateDevice(
        pAdapter, 
        DriverType, 
        Software, 
        Flags, 
        pFeatureLevels, 
        FeatureLevels, 
        SDKVersion, 
        ppDevice, 
        pFeatureLevel, 
        ppImmediateContext
    );
    gDeviceHook.Init(*ppDevice);
    (*ppDevice) = (ID3D11Device*) &gDeviceHook;
    gContextHook.Init(*ppImmediateContext);
    (*ppImmediateContext) = (ID3D11DeviceContext*)&gContextHook;

    return result;
}
